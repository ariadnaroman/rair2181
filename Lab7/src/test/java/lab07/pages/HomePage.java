package lab07.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.bilete.ro/")
public class HomePage extends PageObject {
    @FindBy(xpath = "/html/body/nav/div[2]/div/div[5]/div/div/form/div/input")
    private WebElementFacade searchField;

    @FindBy(xpath = "/html/body/nav/div[2]/div/div[5]/div/div/form/div/button")
    private WebElementFacade searchButton;

    @FindBy(xpath = "//*[@id=\"logoutForm\"]/a")
    private WebElementFacade logoutForm;

    public void enter_keywords(String keyword) {
        searchField.click();
        searchField.type(keyword);
    }

    public void search_events() {
        searchButton.click();
    }

    public String get_logout_form_text() {
        return logoutForm.getText();
    }

//    public List<String> getSuggestions() {
//        WebElementFacade definitionList = find(By.tagName("ol"));
//        return definitionList.findElements(By.tagName("li")).stream()
//                .map( element -> element.getText() )
//                .collect(Collectors.toList());
//    }
}
