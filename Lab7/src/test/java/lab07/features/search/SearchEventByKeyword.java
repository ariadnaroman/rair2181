package lab07.features.search;

import lab07.steps.serenity.EndUserSearchSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;


@RunWith(SerenityRunner.class)
public class SearchEventByKeyword {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSearchSteps endUserSearchSteps;

    @Issue("#SEARCHValid")
    @Test
    public void searching_by_keyword_muzica_should_display_events() {
        endUserSearchSteps.is_home_page();
        endUserSearchSteps.looks_for("muzica");
        endUserSearchSteps.should_see_event("Concert Aniversar Nineta Popa - Orchestra De Muzica Populara");
    }

    @Issue("#SEARCHNotValid")
    @Test
    public void searching_by_keyword_a_should_display_error() {
        endUserSearchSteps.is_home_page();
        endUserSearchSteps.looks_for("a");
        endUserSearchSteps.should_get_error();
    }


} 