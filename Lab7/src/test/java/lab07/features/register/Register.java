package lab07.features.register;

import lab07.steps.serenity.EndUserRegisterSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@RunWith(SerenityRunner.class)
public class Register {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserRegisterSteps endUserRegisterSteps;

    @Issue("#REGISTERValid")
    @Test
    public void register_should_be_successful() {
        String email = "a" + System.currentTimeMillis() + "@yahoo.com";
        String password = "@cest@este1t3st";
        endUserRegisterSteps.is_register_page();
        By byclass = By.className("preloader-overlay done");
        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(byclass));
//        webdriver.manage().window().maximize();
//        JavascriptExecutor jse = (JavascriptExecutor) webdriver;
//        jse.executeScript("window.scrollBy(0,100)");
        endUserRegisterSteps.enters_email(email);
        endUserRegisterSteps.enters_password(password);
        endUserRegisterSteps.confirms_password(password);
        endUserRegisterSteps.starts_register();
        endUserRegisterSteps.should_be_logged_in(email);
    }

    @Issue("#REGISTERNotValid")
    @Test
    public void register_should_get_error() {
        String email = "a";
        String password = "a";
        endUserRegisterSteps.is_register_page();
        endUserRegisterSteps.enters_email(email);
        endUserRegisterSteps.enters_password(password);
        endUserRegisterSteps.confirms_password(password);
        endUserRegisterSteps.starts_register();
        endUserRegisterSteps.should_get_error();
    }


}
