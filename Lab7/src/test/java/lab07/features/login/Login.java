package lab07.features.login;

import lab07.steps.serenity.EndUserLoginSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


@RunWith(SerenityRunner.class)
public class Login {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserLoginSteps endUserLoginSteps;

    @Issue("#LOGINValid")
    @Test
    public void logging_in_should_be_successfull() {
        String email = "roman.ariadna2@gmail.com";
        String password = "@cest@este1t3st";
        endUserLoginSteps.is_login_page();
        By byclass = By.className("preloader-overlay done");
        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(byclass));
//        webdriver.manage().window().maximize();
//        JavascriptExecutor jse = (JavascriptExecutor) webdriver;
//        jse.executeScript("window.scrollBy(0,100)");
        endUserLoginSteps.enters_email(email);
        endUserLoginSteps.enters_password(password);
        endUserLoginSteps.starts_login();
        endUserLoginSteps.should_be_logged_in(email);
    }

    @Issue("#LOGINNotValid")
    @Test
    public void logging_in_should_get_error() {
        String email = "a";
        String password = "a";
        endUserLoginSteps.is_login_page();
        endUserLoginSteps.enters_email(email);
        endUserLoginSteps.enters_password(password);
        endUserLoginSteps.should_get_error();
    }


}