package lab07.steps.serenity;

import lab07.pages.HomePage;
import lab07.pages.RegisterPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EndUserRegisterSteps {
    RegisterPage registerPage;
    HomePage homePage;

    @Step
    public void enters_email(String email) {
        registerPage.enter_email(email);
    }

    @Step
    public void enters_password(String password) {
        registerPage.enter_password(password);
    }

    @Step
    public void confirms_password(String password) {
        registerPage.confirm_password(password);
    }

    @Step
    public void starts_register() {
        registerPage.press_register();
    }

    @Step
    public void should_be_logged_in(String email) {
        assertThat(homePage.get_logout_form_text(), containsString(email));
    }

    @Step
    public void is_register_page() {
        registerPage.open();
    }

    @Step
    public void should_get_error() {
        assertThat(registerPage.getPasswordError(), containsString("Parola trebuie sa aiba minim 6 caractere"));
    }
}
