package lab07.steps.serenity;

import lab07.pages.HomePage;
import lab07.pages.LoginPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EndUserLoginSteps {
    LoginPage loginPage;
    HomePage homePage;

    @Step
    public void enters_email(String email) {
        loginPage.enter_email(email);
    }

    @Step
    public void enters_password(String password) {
        loginPage.enter_password(password);
    }

    @Step
    public void starts_login() {
        loginPage.press_login();
    }

    @Step
    public void should_be_logged_in(String email) {
        assertThat(homePage.get_logout_form_text(), containsString(email));
    }

    @Step
    public void is_login_page() {
        loginPage.open();
    }

    @Step
    public void should_get_error() {
        assertThat(loginPage.getEmailError(), containsString("The Email field is not a valid e-mail address."));
    }
}