package lab07.steps.serenity;

import lab07.pages.EventsPage;
import lab07.pages.HomePage;
import net.thucydides.core.annotations.Step;
import org.hamcrest.Matchers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSearchSteps {

    HomePage homePage;
    EventsPage eventsPage;

    @Step
    public void enters(String keyword) {
        homePage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        homePage.search_events();
    }

    @Step
    public void should_see_event(String event) {
        assertThat(eventsPage.getEvents(), hasItem(containsString(event)));
    }

    @Step
    public void is_home_page() {
        homePage.open();
    }

    @Step
    public void looks_for(String event) {
        enters(event);
        starts_search();
    }

    @Step
    public void should_get_error() {
        System.out.println(eventsPage.getTitle() +  "****");
        assertThat(eventsPage.getTitle(), containsString("500 Problema tehnica"));
    }
}