package evaluator;

import evaluator.controller.IntrebariController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TopDownTest {
    @Test
    public void addIntrebare() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        assertEquals(intrebariRepository.getIntrebari().size(),0);
        intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1","Muzica"));
        assertEquals(intrebariRepository.getIntrebari().size(),1);
    }

    @Test
    public void createNewTest2() throws Exception {
        IntrebariController intrebariController = new IntrebariController();
        Intrebare i1 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca1?","1) 1980","2) 1985","3) 1975","1","Muzica1");
        Intrebare i2 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca2?","1) 1980","2) 1985","3) 1975","1","Muzica2");
        Intrebare i3 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca3?","1) 1980","2) 1985","3) 1975","1","Muzica3");
        Intrebare i4 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca4?","1) 1980","2) 1985","3) 1975","1","Muzica4");
        Intrebare i5 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca5?","1) 1980","2) 1985","3) 1975","1","Muzica5");
        intrebariController.addNewIntrebare(i1);
        intrebariController.addNewIntrebare(i2);
        intrebariController.addNewIntrebare(i3);
        intrebariController.addNewIntrebare(i4);
        intrebariController.addNewIntrebare(i5);
        evaluator.model.Test test = intrebariController.createNewTest();
        assertEquals(test.getIntrebari().size(),5);
    }

    @Test
    public void createStatisticaValid() throws InputValidationFailedException, DuplicateIntrebareException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        IntrebariController intrebariController = new IntrebariController();
        Intrebare i1 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca1?","1) 1980","2) 1985","3) 1975","1","Muzica1");
        Intrebare i2 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca2?","1) 1980","2) 1985","3) 1975","1","Muzica2");
        Intrebare i3 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca3?","1) 1980","2) 1985","3) 1975","1","Muzica3");
        Intrebare i4 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca4?","1) 1980","2) 1985","3) 1975","1","Muzica4");
        Intrebare i5 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca5?","1) 1980","2) 1985","3) 1975","1","Muzica5");
        intrebariController.addNewIntrebare(i1);
        intrebariController.addNewIntrebare(i2);
        intrebariController.addNewIntrebare(i3);
        intrebariController.addNewIntrebare(i4);
        intrebariController.addNewIntrebare(i5);
        Map<String,Integer> statistica = intrebariController.getStatistica();
        assertEquals((int)statistica.get("Muzica1"), 1);
        assertEquals((int)statistica.get("Muzica2"), 1);
        assertEquals((int)statistica.get("Muzica3"), 1);
        assertEquals((int)statistica.get("Muzica4"), 1);
        assertEquals((int)statistica.get("Muzica5"), 1);
    }

    @Test
    public void topDown1() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException, NotAbleToCreateTestException {
        IntrebariController intrebariController = new IntrebariController();
        Intrebare i1 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca1?","1) 1980","2) 1985","3) 1975","1","Muzica1");
        intrebariController.addNewIntrebare(i1);
        assertTrue(intrebariController.exists(i1));
    }

    @Test
    public void topDown2() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException, NotAbleToCreateTestException {
        IntrebariController intrebariController = new IntrebariController();
        Intrebare i1 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca1?","1) 1980","2) 1985","3) 1975","1","Muzica1");
        Intrebare i2 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca2?","1) 1980","2) 1985","3) 1975","1","Muzica2");
        Intrebare i3 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca3?","1) 1980","2) 1985","3) 1975","1","Muzica3");
        Intrebare i4 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca4?","1) 1980","2) 1985","3) 1975","1","Muzica4");
        Intrebare i5 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca5?","1) 1980","2) 1985","3) 1975","1","Muzica5");
        intrebariController.addNewIntrebare(i1);
        intrebariController.addNewIntrebare(i2);
        intrebariController.addNewIntrebare(i3);
        intrebariController.addNewIntrebare(i4);
        intrebariController.addNewIntrebare(i5);
        assertTrue(intrebariController.exists(i1));
        assertTrue(intrebariController.exists(i2));
        assertTrue(intrebariController.exists(i3));
        assertTrue(intrebariController.exists(i4));
        assertTrue(intrebariController.exists(i5));
        evaluator.model.Test test = intrebariController.createNewTest();
        assertEquals(test.getIntrebari().size(),5);
    }

    @Test
    public void topDown3() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException, NotAbleToCreateTestException {
        IntrebariController intrebariController = new IntrebariController();
        Intrebare i1 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca1?","1) 1980","2) 1985","3) 1975","1","Muzica1");
        Intrebare i2 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca2?","1) 1980","2) 1985","3) 1975","1","Muzica2");
        Intrebare i3 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca3?","1) 1980","2) 1985","3) 1975","1","Muzica3");
        Intrebare i4 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca4?","1) 1980","2) 1985","3) 1975","1","Muzica4");
        Intrebare i5 = new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca5?","1) 1980","2) 1985","3) 1975","1","Muzica5");
        intrebariController.addNewIntrebare(i1);
        intrebariController.addNewIntrebare(i2);
        intrebariController.addNewIntrebare(i3);
        intrebariController.addNewIntrebare(i4);
        intrebariController.addNewIntrebare(i5);
        assertTrue(intrebariController.exists(i1));
        assertTrue(intrebariController.exists(i2));
        assertTrue(intrebariController.exists(i3));
        assertTrue(intrebariController.exists(i4));
        assertTrue(intrebariController.exists(i5));
        evaluator.model.Test test = intrebariController.createNewTest();
        assertEquals(test.getIntrebari().size(),5);
        Map<String,Integer> statistica = intrebariController.getStatistica();
        assertEquals((int)statistica.get("Muzica1"), 1);
        assertEquals((int)statistica.get("Muzica2"), 1);
        assertEquals((int)statistica.get("Muzica3"), 1);
        assertEquals((int)statistica.get("Muzica4"), 1);
        assertEquals((int)statistica.get("Muzica5"), 1);
    }
}
