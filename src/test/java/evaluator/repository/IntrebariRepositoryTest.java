package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTest {
    @Test
    public void addIntrebare() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        assertEquals(intrebariRepository.getIntrebari().size(),0);
        intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1","Muzica"));
        assertEquals(intrebariRepository.getIntrebari().size(),1);
    }

    @Test
    public void addIntrebare1() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        try {
            intrebariRepository.addIntrebare(new Intrebare("","1) 1980","2) 1985","3) 1975","1","Muzica"));
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addIntrebare2() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        try {
            intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Procaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa?","1) 1980","2) 1985","3) 1975","1","Muzica"));
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addIntrebare3() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        try {
            intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1","muzica"));
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addIntrebare4() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        assertEquals(intrebariRepository.getIntrebari().size(),0);
        intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1","A"));
        assertEquals(intrebariRepository.getIntrebari().size(),1);
    }

    @Test
    public void addIntrebare5() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        try {
            intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1",""));
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addIntrebare6() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1","Ab"));
    }

    @Test
    public void addIntrebare7() throws Exception {
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        try {
            intrebariRepository.addIntrebare(new Intrebare("In ce an s-a nascut compozitorul Andrei Mihai Proca?","1) 1980","2) 1985","3) 1975","1","DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD123"));
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
    }
}