package evaluator.model;

import evaluator.util.InputValidation;
import evaluator.exception.InputValidationFailedException;

public class Intrebare {

	private String enunt;
	private String varianta1;
	private String varianta2;
	private String varianta3;
	private String variantaCorecta;
	private String domeniu;
	
	public Intrebare() {
	}
	
	public Intrebare(String enunt, String varianta1, String varianta2, String varianta3,
			String variantaCorecta, String domeniu) throws InputValidationFailedException {

		
		this.enunt = enunt;
		this.varianta1 = varianta1;
		this.varianta2 = varianta2;
		this.varianta3 = varianta3;
		this.variantaCorecta = variantaCorecta;
		this.domeniu = domeniu;
	}


	public String getEnunt() {
		return enunt;
	}
	public void setEnunt(String enunt) {
		this.enunt = enunt;
	}
	public String getVarianta1() {
		return varianta1;
	}
	public void setVarianta1(String varianta1) {
		this.varianta1 = varianta1;
	}
	public String getVarianta2() {
		return varianta2;
	}
	public void setVarianta2(String varianta2) {
		this.varianta2 = varianta2;
	}
	public String getVariantaCorecta() {
		return variantaCorecta;
	}
	public void setVariantaCorecta(String variantaCorecta) {
		this.variantaCorecta = variantaCorecta;
	}
	public String getDomeniu() {
		return domeniu;
	}
	public void setDomeniu(String domeniu) {
		this.domeniu = domeniu;
	}
	public String getVarianta3() {
		return varianta3;
	}
	public void setVarianta3(String varianta3) {
		this.varianta3 = varianta3;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Intrebare intrebare = (Intrebare) o;

		if (enunt != null ? !enunt.equals(intrebare.enunt) : intrebare.enunt != null) return false;
		if (varianta1 != null ? !varianta1.equals(intrebare.varianta1) : intrebare.varianta1 != null) return false;
		if (varianta2 != null ? !varianta2.equals(intrebare.varianta2) : intrebare.varianta2 != null) return false;
		if (varianta3 != null ? !varianta3.equals(intrebare.varianta3) : intrebare.varianta3 != null) return false;
		if (variantaCorecta != null ? !variantaCorecta.equals(intrebare.variantaCorecta) : intrebare.variantaCorecta != null)
			return false;
		return domeniu != null ? domeniu.equals(intrebare.domeniu) : intrebare.domeniu == null;
	}

	@Override
	public int hashCode() {
		int result = enunt != null ? enunt.hashCode() : 0;
		result = 31 * result + (varianta1 != null ? varianta1.hashCode() : 0);
		result = 31 * result + (varianta2 != null ? varianta2.hashCode() : 0);
		result = 31 * result + (varianta3 != null ? varianta3.hashCode() : 0);
		result = 31 * result + (variantaCorecta != null ? variantaCorecta.hashCode() : 0);
		result = 31 * result + (domeniu != null ? domeniu.hashCode() : 0);
		return result;
	}
}
